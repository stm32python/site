# Introduction

Ce markdown sert à centraliser toutes les informations permettant de modifier le site dans sa structure (et non dans son contenu). <br/>
Il y'a différents chapitres traitant différents aspects du site. Dans chaque chapitre vous trouverez des informations sur comment modifier les différents éléments du site. Nottament où trouver les fichier sources et une explication de leur structure.

/!\\ Ce document n'est pas exhaustif, il rassemble les connaissances accumulées par les contributeurs du développement du site au cours de leurs modification de ce dernier.

## Une documentation par le créateur du thème.

Pour tout ce qui se réfère au thème Jekyll. <br/>
Vous trouverez bon nombre d'information sur le site de TeXt Theme : <br/>
https://tianqi.name/jekyll-TeXt-theme/ <br/>
Dans la rubrique docs nottament. <br/>
La page GitHub du theme pourra également vous aider dans vos recherches :
https://github.com/kitian616/jekyll-TeXt-theme <br/>

## Header

#### Fichiers et dossiers
* _includes/header.html
* _data/navigation.yml
* _includes/svg/
* _config.yml
* site/assets/flags/
* _sass/components/_header.scss

#### Modification des logos
Les logos sont dans la `div` de classe `header__brand` (ligne 10 dans `header.html`). <br/>
Ils peuvent être ajoutés via `include` si le logo se trouve dans `includes.svg`. <br/>
Autrement ils peuvent être ajouté via `img`.
#### Modification du titre
Le titre se trouve dans le `_config.yml` à la ligne 21.
#### Modification des pages accessibles
La liste des pages accessibles via le header se trouve dans le fichier `navigation.yml` dans la catégorie `header` ligne 1.
#### Modification du lien gitlab et des accès à d'autres langues
Le lien gitlab est dans le header.html dans `nav class="navigation"` à la ligne 42.
La liste des autres langues disponible avec leur logo et le lien du site associé se situe dans le fichier `_config.yml` dans la catégorie `language` à la ligne 41.

## Footer navigation

#### Fichier et dossiers

* _includes/article-section-navigation.html

#### Affichier la navigation en pied de page.

Il est possible d'avoir en pied de page un bouton pour l'article suivant
et l'article précédent. <br/>
Le code qui le génère se trouve dans `article-section-navigation.html` à la ligne 37. <br/>
Cette navigation dépend de la sidebar. Et demandera un travail conséquent pour faire en sorte que les next et previous pointent sur d'autre pages que celles prévu par la navigation sidebar.
