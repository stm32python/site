---
locale: 'fr'
layout : article
title: MicroPython
description: MicroPython
permalink: /fr/microPython.html
cover: site/assets/cover/microPython/upython.png
key: page-aside
aside:
  toc: true
sidebar:
  nav: site_nav
---

<img align="center" src="site/assets/images/microPython/upython.png" alt="drawing" width="100"/>

**MicroPython** est une implémentation légère et efficace du langage de programmation Python 3 incluant un petit sous-ensemble de la bibliothèque standard Python et optimisée pour fonctionner sur des microcontrôleurs et dans des environnements contraints.

MicroPython regorge de fonctionnalités avancées telles qu'une invite en lignes de commandes, la capacité de réaliser des opérations mathématique sur des entiers de taille arbitraire, la gestion de listes, la gestion des exceptions et plus encore. Pourtant, il est suffisamment compact pour tenir et fonctionner dans seulement 256 Ko d'espace de code et 16 Ko de RAM.

* [Site web](http://micropython.org/)
* [Bibliothèques](http://docs.micropython.org/en/latest/library/index.html)
