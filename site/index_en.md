---
layout: article
title: STM32python - HOME
permalink: en/
key: home
mode: immersive
locale: 'en'
article_header:
  actions:
    - text: Getting Started
      type: success
      url: "en/#themes"
  type: overlay
  theme: dark
  background_color: '#203028'
  background_image:
  #  gradient: 'linear-gradient(135deg, rgba(34, 139, 87 , .4), rgba(139, 34, 139, .4))'
    src: /site/assets/cover/home_page/home.jpg

---

# Welcome to STM32MicroPython

<p align="center">
<img src="{{site.baseurl}}/assets/logos/stm32upython.svg" width="400px" alt="logo"/>
</p>

## Context

The reform of french high schools introduces a new education followed by all
students of [general and technological "seconde" year](https://en.wikipedia.org/wiki/Secondary_education_in_France)
(corresponding to [US 10th Grade or UK Year 11](https://en.wikipedia.org/wiki/Secondary_school)) :
[SNT (Digital Sciences and Technology)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
One of the topics covered in this course is the [Internet of Things](https://en.wikipedia.org/wiki/Internet_of_things) (IoT) covered into the chapter "Informatique embarquée et objets connectés",
which represents the extension of the Internet to things and places into the physical world.

The aim is to bring these young people to a first level of understanding of the Internet of Things. The challenge is to favor a chosen study orientation, in this case towards digital engineering. The share of "digital" and "IT" in education has been greatly increased with the reform of the school.

## Goal

The goal of STM32Python project is to provide high school teachers and high school students with open-source teaching materials
for initiation to the Internet of Things for the teaching of 
[SNT (Digital Sciences and Technology)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
These supports are based on the Nucleo STM32 platform from ST Microelectronics. They allow electronic assemblies and programs for STM32 microcontrollers to be made with the C / C ++ and microPython languages.

The supports produced can also be used by other general first and final lessons, in particular in NSI specialty (Digital and Computer Sciences), in IS specialty (Engineering Sciences), or in STI2D technological series (Sciences and Technologies of 'Industry and Sustainable Development).


<div class="layout--articles">
  <section class="my-5">
    <header><h2 id = "themes">Les Thèmes :</h2></header>
    {%- include article-list.html articles=site.pages lang=page.locale type='grid' -%}
  </section>
</div>

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="article-layout">Autres liens</h2></header>
    {%- include article-list.html articles=site.about lang=page.locale type='grid' -%}
  </section>
</div>

## Partners
The partners of the STM32Python project are:
* les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr),
* [ST Microelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).
