---
layout: article
locale: 'fr'
title: Buzzer en C/C++
description: Exercice avec le buzzer en C/C++ pour Stm32duino
permalink: fr/buzzer.html
key: page-aside
cover: site/assets/cover/duino/buzzer.png
aside:
  toc: true
sidebar:
  nav: site_nav
---
**Même prérequis que pour le tilt-sensor.**


**Le Buzzer :**


Le buzzer vibre et produit un son lorsque on lui transmet une tension. Il est possible de modifier la fréquence du son.

![Image](site/assets/images/stm32duino/buzzer.png)


*Voici le code sur Arduino*
```c
int frequence[] = {262, 294, 330, 349, 370, 392, 440, 494};      

void setup()
{
  Serial.begin(9600);
  pinMode(D4,OUTPUT);             //le pin du buzzer est défini en sortie
}

void loop()
{
  for (int i = 0; i <= 8; i++)    //on parcour les 8 fréquence définies dans le tableau plus haut
  {
  tone(D4, frequence[i], 500);    //tone(Pin, frequence, durée)
  noTone(D4);                     //stop le son sur le pin concerné                        
  delay(500);                                                                                   
  }
}
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
