---
locale: 'fr'
layout: article
title: STM32duino
description: Section STM32duino
permalink: /fr/stm32duino.html
key: page-aside
cover: site/assets/cover/home_page/STM32duino.jpg
aside:
  toc: true
sidebar:
  nav: site_nav

---

[STM32duino](https://github.com/stm32duino) est un projet rassemblant des bibliothèques Arduino pour les cartes de développement STM32 (Nucleo et Discovery) et pour les composants MEMS de STMicroelectronics. Il permet de développer et compiler des programmes (ie. _sketch_) depuis l'environnement de développement Arduino IDE.

# Sommaire

Vous trouverez dans cette partie tous les exercices [STM32duino](https://github.com/stm32duino) pour le kit pédagogique STM32python.

Avant toute chose, assurez vous d'avoir une installation fonctionnelle.
Le protocole à suivre pour ce faire est détaillé dans la section **installation**.

<div class="layout--articles">
	<section class="my-5">
		<header><h2>Installation </h2></header>
	  {%- include article-list.html articles=site.duino_install lang=page.locale type='grid' -%}
	</section>
</div>

<div class="layout--articles">
	<section class="my-5">
		<header><h2>Tutoriels </h2></header>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Afficheurs </h3></header>
					{%- include article-list.html articles=site.duino_layers lang=page.locale type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Capteurs </h3></header>
					{%- include article-list.html articles=site.duino_sensors lang=page.locale type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Enregistrement de données </h3></header>
					{%- include article-list.html articles=site.duino_download_data lang=page.locale type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Actuateurs </h3></header>
					{%- include article-list.html articles=site.duino_actuators lang=page.locale type='grid' -%}
			</section>
		</div>
		<div class="layout--articles">
			<section class="my-5">
				<header><h3>Autre </h3></header>
					{%- include article-list.html articles=site.duino_other lang=page.locale type='grid' -%}
			</section>
		</div>
	</section>
</div>
